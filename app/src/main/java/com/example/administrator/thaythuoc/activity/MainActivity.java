package com.example.administrator.thaythuoc.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.administrator.thaythuoc.R;
import com.example.administrator.thaythuoc.Utils;

public class MainActivity  extends AppCompatActivity implements View.OnClickListener {
    private MBroadcast mBroadcast;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initsView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBroadcast();
    }

    private void registerBroadcast() {
        mBroadcast = new MBroadcast();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(mBroadcast,intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcast);
        mBroadcast = null;
    }

    private void initsView() {
       findViewById(R.id.btn_chat_bot).setOnClickListener(this);
        findViewById(R.id.btn_dong_y).setOnClickListener(this);
        findViewById(R.id.btn_che_do_an).setOnClickListener(this);
        findViewById(R.id.btn_bmi).setOnClickListener(this);
        findViewById(R.id.btn_hoi_chung_benh).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_dong_y:
                Intent iDongY= new Intent(MainActivity.this,DongyActivity.class);
                startActivity(iDongY);
                overridePendingTransition(R.anim.enter,R.anim.exist);


                break;

            case R.id.btn_che_do_an:
                Intent iChe= new Intent(MainActivity.this,CheDoAnActivity.class);
                startActivity(iChe);
                overridePendingTransition(R.anim.enter,R.anim.exist);
                break;
        }

    }

    private class MBroadcast extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action){
                case "android.net.conn.CONNECTIVITY_CHANGE":
                    if (!Utils.isConnected(getApplication())) {
                        Toast.makeText(MainActivity.this, "The internet connect fail", Toast.LENGTH_SHORT).show();
                    }
            }
        }
    }
}
