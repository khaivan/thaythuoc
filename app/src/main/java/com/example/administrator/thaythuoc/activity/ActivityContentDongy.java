package com.example.administrator.thaythuoc.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.thaythuoc.R;
import com.example.administrator.thaythuoc.Utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class ActivityContentDongy extends AppCompatActivity {
    private WebView webView;
    private String filePath;
    private TextView tvTitle;
    private ImageButton iBtnBack;
    private String url;
    String html = "Loading...";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content);
        initsView();


        Intent intent = getIntent();
        filePath = intent.getStringExtra("B");
        tvTitle.setText(intent.getStringExtra("A"));
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        //Display "Loading..." message while waiting
        webView.loadData(html, "text/html; charset=UTF-8", null);
        //Invoke the AsyncTask

        new GetData().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initsView() {
        webView = findViewById(R.id.tv_content);
        tvTitle = findViewById(R.id.tv_title);
        iBtnBack = findViewById(R.id.btn_back);
        iBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.enter, R.anim.exist);
            }
        });
    }

    private class GetData extends AsyncTask<Void, Void, String> {

        // This is run in a background thread
        @Override
        protected String doInBackground(Void... params) {
            try {


                Document doc = Jsoup.connect(filePath)
                        .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0")
                        .get();
                Elements ele = doc.select("div[id*=6]");
                html = ele.toString();
                return html;
            } catch (Exception e) {
//
//                Log.d("APP", e.toString());
            }
            return "Không có kết nối internet";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

//            WebView.loadData(result, mime, encoding);
            webView.loadData(result, "text/html; charset=UTF-8", null);
        }
    }




}
