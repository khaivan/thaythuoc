package com.example.administrator.thaythuoc.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.thaythuoc.R;
import com.example.administrator.thaythuoc.database.DataAnUong;
import com.example.administrator.thaythuoc.database.ListAlpha;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.administrator.thaythuoc.R.color.chedoan;

public class AdapterCheDoAn extends RecyclerView.Adapter<AdapterCheDoAn.ViewHolder> {
    private IGetAdapterACheDoAn iGet;

    public AdapterCheDoAn(IGetAdapterACheDoAn iGet) {
        this.iGet = iGet;
    }

    @NonNull
    @Override
    public AdapterCheDoAn.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_che_do_an,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCheDoAn.ViewHolder holder, final int position) {
        DataAnUong dataAnUong = iGet.getItems(position);
        holder.tv_title.setText(dataAnUong.getName());
//        holder.img.setImageResource(R.drawable.chedoan);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGet.onClickItem(position);
                Log.d("a", "onClick: "+position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return iGet.getCount();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_title;
//        private CircleImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_alpha);
//            img = itemView.findViewById(R.id.img);

        }
    }
    public interface IGetAdapterACheDoAn{
        int getCount ();
        DataAnUong getItems(int position);
        void onClickItem(int position);
    }
}
