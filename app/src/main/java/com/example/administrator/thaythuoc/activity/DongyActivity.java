package com.example.administrator.thaythuoc.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.administrator.thaythuoc.R;
import com.example.administrator.thaythuoc.adapter.AdapterListAlpha;
import com.example.administrator.thaythuoc.adapter.AdapterTenThuoc;
import com.example.administrator.thaythuoc.database.DatabaseDongY;
import com.example.administrator.thaythuoc.database.DatabaseManager;
import com.example.administrator.thaythuoc.database.ListAlpha;

import java.util.ArrayList;
import java.util.List;

public class DongyActivity extends AppCompatActivity implements AdapterListAlpha.IGetAdapterAlpha, AdapterTenThuoc.IGetAdapterA, View.OnClickListener {

    private RecyclerView rcAlpha;
    private ImageButton btnBack, btnBackHome;
    private TextView tvTitle;
    private ActionBar actionBar;
    private AdapterListAlpha aDapterListAlpha;
    private AdapterTenThuoc adapterTenThuoc;
    private DatabaseManager databaseManager;
    private List<ListAlpha> listAlphas;
    private SearchView search;
    private Toolbar toolbarMain;
    private String query;
    private List<ListAlpha> listResultAlP;
    private List<DatabaseDongY> listReDatabases;
    private int index = 0;
    private int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dongy);
        inistView();
//        inits();
        initToolbar();
        databaseManager = new DatabaseManager(this);
//        databaseManager.databaseList();
        aDapterListAlpha = new AdapterListAlpha(this);
        initsListAlph();
        btnBack.setVisibility(View.INVISIBLE);
        btnBackHome.setVisibility(View.VISIBLE);
        rcAlpha.setLayoutManager(new LinearLayoutManager(this));
        rcAlpha.setAdapter(aDapterListAlpha);
        Log.d("", "onCreate: ");

    }

    private void initsListAlph() {
        listAlphas = new ArrayList<>();
        ListAlpha l1 = new ListAlpha("Đông y chữa bệnh vần A ",1);
        ListAlpha l2 = new ListAlpha("Đông y chữa bệnh vần B ",2);
        ListAlpha l3 = new ListAlpha("Đông y chữa bệnh vần C ",3);
        ListAlpha l4 = new ListAlpha("Đông y chữa bệnh vần D ",4);
        ListAlpha l5 = new ListAlpha("Đông y chữa bệnh vần Đ ",5);
        ListAlpha l6 = new ListAlpha("Đông y chữa bệnh vần E ",6);
        ListAlpha l7 = new ListAlpha("Đông y chữa bệnh vần G ",7);
        ListAlpha l8 = new ListAlpha("Đông y chữa bệnh vần H ",8);
        ListAlpha l9 = new ListAlpha("Đông y chữa bệnh vần I ",9);
        ListAlpha l10 = new ListAlpha("Đông y chữa bệnh vần K ",10);
        ListAlpha l11 = new ListAlpha("Đông y chữa bệnh vần L ",11);
        ListAlpha l12 = new ListAlpha("Đông y chữa bệnh vần M ",12);
        ListAlpha l13 = new ListAlpha("Đông y chữa bệnh vần N ",13);
        ListAlpha l14 = new ListAlpha("Đông y chữa bệnh vần O ",14);
        ListAlpha l15 = new ListAlpha("Đông y chữa bệnh vần P ",15);
        ListAlpha l16 = new ListAlpha("Đông y chữa bệnh vần Q ",16);
        ListAlpha l17 = new ListAlpha("Đông y chữa bệnh vần R ",17);
        ListAlpha l18 = new ListAlpha("Đông y chữa bệnh vần S ",18);
        ListAlpha l19 = new ListAlpha("Đông y chữa bệnh vần T ",19);
        ListAlpha l20 = new ListAlpha("Đông y chữa bệnh vần U ",20);
        ListAlpha l21 = new ListAlpha("Đông y chữa bệnh vần V ",21);
        ListAlpha l22 = new ListAlpha("Đông y chữa bệnh vần X ",22);
        ListAlpha l23 = new ListAlpha("Đông y chữa bệnh vần Z ",23);
        listAlphas.add(l1);
        listAlphas.add(l2);
        listAlphas.add(l3);
        listAlphas.add(l4);
        listAlphas.add(l5);
        listAlphas.add(l6);
        listAlphas.add(l7);
        listAlphas.add(l8);
        listAlphas.add(l9);
        listAlphas.add(l10);
        listAlphas.add(l11);
        listAlphas.add(l12);
        listAlphas.add(l13);
        listAlphas.add(l14);
        listAlphas.add(l15);
        listAlphas.add(l16);
        listAlphas.add(l17);
        listAlphas.add(l18);
        listAlphas.add(l19);
        listAlphas.add(l20);
        listAlphas.add(l21);
        listAlphas.add(l22);
        listAlphas.add(l23);
    }

    private void initToolbar() {
        actionBar = getSupportActionBar();
        toolbarMain = findViewById(R.id.toolbar);
        setSupportActionBar(toolbarMain);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);


    }

    private void inistView() {
        rcAlpha = findViewById(R.id.rc_alpha);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnBackHome = findViewById(R.id.btn_back_home);
        btnBackHome.setOnClickListener(this);
        tvTitle = findViewById(R.id.tv_title);
    }

    @Override
    public ListAlpha getItems(int position) {
        return listAlphas.get(position);
    }

    @Override
    public int getCount() {
        return listAlphas.size();
    }

    @Override
    public int getCountTenThuoc() {
        return databaseManager.getDatabases().size();
    }

    @Override
    public DatabaseDongY getItem(int position) {
        if (databaseManager == null) {
            return null;
        }
        return databaseManager.getDatabases().get(position);
    }

    @Override
    public void onClickItemTenThuoc(int position) {
        Intent iContent = new Intent(DongyActivity.this, ActivityContentDongy.class);
        iContent.putExtra("A", databaseManager.databases.get(position).getName());
        iContent.putExtra("B", databaseManager.databases.get(position).getLink());
        startActivity(iContent);
        overridePendingTransition(R.anim.enter,R.anim.exist);

    }


    @Override
    public void onClickItem(int position) {
        index = 1;
        i = position + 1;
        databaseManager.getListDongY(listAlphas.get(position).getId());
        setAdapter();
        search.setQueryHint(getString(R.string.searchName));
        tvTitle.setText(listAlphas.get(position).getName());
        btnBack.setVisibility(View.VISIBLE);
        btnBackHome.setVisibility(View.INVISIBLE);
    }

    private void setAdapter() {
        adapterTenThuoc = new AdapterTenThuoc(this);
        rcAlpha.setLayoutManager(new LinearLayoutManager(this));
        rcAlpha.setAdapter(adapterTenThuoc);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                index = 0;
                tvTitle.setText("Bài thuốc đông y");
                rcAlpha.setAdapter(aDapterListAlpha);
                search.setQueryHint(getString(R.string.searchalpha));
                btnBackHome.setVisibility(View.VISIBLE);
                btnBack.setVisibility(View.INVISIBLE);
                break;
            case R.id.btn_back_home:
               onBackPressed();
                overridePendingTransition(R.anim.enter,R.anim.exist);

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
//        btnFilt = (Button) menu.findItem(R.id.action_filt).getActionView();
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setQueryHint(getString(R.string.searchalpha));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                switch (index) {
                    case 0:

                        getFilter().filter(s);
                        break;

                    case 1:

                        getFilterName().filter(s);
                        break;

                    default:
                        break;

                }
                return true;
            }
        });
        search.onActionViewCollapsed();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search: {
                supportInvalidateOptionsMenu();
                return true;
            }
//            case R.id.action_filt:
//                Toast.makeText(this, "action_filt", Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private FilterAlpha filter = new FilterAlpha();

    public FilterAlpha getFilter() {
        return filter;
    }

    //filter theo van
    private class FilterAlpha extends android.widget.Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            initsListAlph();

            query = charSequence.toString().toLowerCase();
            listResultAlP = new ArrayList<>();
            FilterResults results = new FilterResults();
            if (query.equals("")) {
                initsListAlph();
            }
            for (ListAlpha l : listAlphas
                    ) {
                String str = l.getName().substring(l.getName().length() - 2, l.getName().length() - 1).toLowerCase();
                Log.d("TAG", "performFiltering: " + str);
                if (str.contains(query)) {
                    listResultAlP.add(l);
                }
            }
            results.values = listResultAlP;
            results.count = listResultAlP.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            listAlphas = (List<ListAlpha>) filterResults.values;
            aDapterListAlpha.notifyDataSetChanged();
        }
    }

    private FilterName filterName = new FilterName();

    public FilterName getFilterName() {
        return filterName;
    }

    public void setFilterName(FilterName filterName) {
        this.filterName = filterName;
    }

    //filter ten thuoc
    private class FilterName extends android.widget.Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            databaseManager.getListDongY(i);

            query = charSequence.toString().toLowerCase();
            listReDatabases = new ArrayList<>();
            FilterResults results = new FilterResults();
            if (query.equals("")) {
                databaseManager.getListDongY(i);
            }
            for (DatabaseDongY l : databaseManager.getDatabases()
                    ) {
                String str = l.getName().toLowerCase();
                if (str.contains(query)) {
                    listReDatabases.add(l);
                }
            }
            results.values = listReDatabases;
            results.count = listReDatabases.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            databaseManager.setDatabases((List<DatabaseDongY>) filterResults.values);
            adapterTenThuoc.notifyDataSetChanged();
        }
    }

}
