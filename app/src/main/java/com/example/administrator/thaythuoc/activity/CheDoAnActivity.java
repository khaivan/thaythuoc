package com.example.administrator.thaythuoc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.thaythuoc.R;
import com.example.administrator.thaythuoc.adapter.AdapterCheDoAn;
import com.example.administrator.thaythuoc.database.DataAnUong;
import com.example.administrator.thaythuoc.database.DatabaseDongY;
import com.example.administrator.thaythuoc.database.DatabaseManager;
import com.example.administrator.thaythuoc.database.ListAlpha;

import java.util.ArrayList;
import java.util.List;

public class CheDoAnActivity extends AppCompatActivity implements View.OnClickListener, AdapterCheDoAn.IGetAdapterACheDoAn {
    private RecyclerView rcAlpha;
    private ImageButton btnBack, btnBackHome;
    private TextView tvTitle;
    private ActionBar actionBar;
    private DatabaseManager databaseManager;
    private SearchView search;
    private Toolbar toolbarMain;
    private AdapterCheDoAn adapterCheDoAn;
    private String query;
    private LinearLayout linearLayout;
    private List<DataAnUong> listReAnUongs;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activyti_che_do_an);


        inistView();

        databaseManager = new DatabaseManager(this);
        databaseManager.getListAnUong();
        initsAdapter();
        initToolbar();

    }

    private void initsAdapter() {
        adapterCheDoAn = new AdapterCheDoAn(this);
        rcAlpha.setLayoutManager(new LinearLayoutManager(this));
        rcAlpha.setAdapter(adapterCheDoAn);
    }

    private void initToolbar() {
        actionBar = getSupportActionBar();
        toolbarMain = findViewById(R.id.toolbar);
        setSupportActionBar(toolbarMain);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);


    }


    private void inistView() {
        rcAlpha = findViewById(R.id.rc_alpha);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnBackHome = findViewById(R.id.btn_back_home);
        btnBackHome.setOnClickListener(this);
        tvTitle = findViewById(R.id.tv_title);
        linearLayout = findViewById(R.id.content_layout);
    }


    @Override
    public void onClick(View view) {
        onBackPressed();
        overridePendingTransition(R.anim.enter,R.anim.exist);


    }

    @Override
    public int getCount() {
        if (databaseManager == null) {
            return 0;
        }
        return databaseManager.getDataAnUongs().size();
    }

    @Override
    public DataAnUong getItems(int position) {
        return databaseManager.getDataAnUongs().get(position);
    }

    @Override
    public void onClickItem(int position) {
//        search.setQueryHint(getString(R.string.searchName));
        Intent iContent = new Intent(CheDoAnActivity.this, ActivityContentCheDoAn.class);
        iContent.putExtra("A", databaseManager.dataAnUongs.get(position).getName());
        iContent.putExtra("B", databaseManager.dataAnUongs.get(position).getLink());
        startActivity(iContent);
        overridePendingTransition(R.anim.enter,R.anim.exist);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
//        btnFilt = (Button) menu.findItem(R.id.action_filt).getActionView();
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();

        search.setQueryHint(getString(R.string.searchCheDA));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                getFilterName().filter(s);
                return true;
            }
        });
        search.onActionViewCollapsed();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search: {
                supportInvalidateOptionsMenu();
                return true;
            }
//            case R.id.action_filt:
//                Toast.makeText(this, "action_filt", Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    //filter theo van
    private FilterName filterName = new FilterName();

    public FilterName getFilterName() {
        return filterName;
    }

    public void setFilterName(FilterName filterName) {
        this.filterName = filterName;
    }

    //filter ten thuoc
    private class FilterName extends android.widget.Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            databaseManager.getListAnUong();

            query = charSequence.toString().toLowerCase();
            listReAnUongs = new ArrayList<>();
            FilterResults results = new FilterResults();
            if (query.equals("")) {
                databaseManager.getListAnUong();
            }
            for (DataAnUong l : databaseManager.getListAnUong()
                    ) {
                String str = l.getName().toLowerCase();
                if (str.contains(query)) {
                    listReAnUongs.add(l);
                }
            }
            results.values = listReAnUongs;
            results.count = listReAnUongs.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            databaseManager.setDataAnUongs((List<DataAnUong>) filterResults.values);
            adapterCheDoAn.notifyDataSetChanged();
        }
    }


}
