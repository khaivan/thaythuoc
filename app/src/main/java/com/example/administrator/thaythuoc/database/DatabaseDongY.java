package com.example.administrator.thaythuoc.database;

public class DatabaseDongY {
    private String name;
    private int id;
private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public DatabaseDongY(String name, int id, String link) {
        this.name = name;
        this.id = id;
        this.link = link;
    }

    public DatabaseDongY() {
    }
}
