package com.example.administrator.thaythuoc.database;

public class ListAlpha {
    private String name;
private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ListAlpha(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public ListAlpha() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
