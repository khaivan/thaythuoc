package com.example.administrator.thaythuoc.database;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {
    private Context context;
    private final String pathDb;
    private String DB_NAME = "dongy.sqlite";
    private SQLiteDatabase sqliteManager;
    private Cursor c;
    private DatabaseDongY database;
    private DataAnUong dataAnUong;

    public List<DataAnUong> getDataAnUongs() {
        return dataAnUongs;
    }

    public void setDataAnUongs(List<DataAnUong> dataAnUongs) {
        this.dataAnUongs = dataAnUongs;
    }

    private int id;
    private String name, link;
    public List<DatabaseDongY> databases;
    public List<DataAnUong> dataAnUongs;

    public List<DatabaseDongY> getDatabases() {
        return databases;
    }

    public void setDatabases(List<DatabaseDongY> databases) {
        this.databases = databases;
    }

    public DatabaseManager(Context context) {
        this.context = context;
        pathDb = Environment.getDataDirectory()
                + File.separator + "data"
                + File.separator + context.getPackageName()
                + File.separator + "database"
                + File.separator + DB_NAME;

    }

    private void coppyDB() {
        String path
                = Environment.getDataDirectory()
                //duong dan external cua toan bo app
                + File.separator + "data"
                + File.separator + context.getPackageName()
                //duong dan external cua app hien tai
                + File.separator + "database";
        new File(path).mkdir();
        File file = new File(pathDb);
        if (file.exists()) {
            return;
        }
        //copy
        AssetManager manager = context.getAssets();
        try {
            InputStream in = manager.open(DB_NAME);
            OutputStream out = new FileOutputStream(file);
            byte[] b = new byte[1024];
            int le = in.read(b);
            while (le > -1) {
                out.write(b, 0, le);
                le = in.read(b);
            }
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openDB() {
        coppyDB();
        if (sqliteManager == null ||
                !sqliteManager.isOpen()) {
            sqliteManager = SQLiteDatabase
                    .openDatabase(pathDb, null,
                            SQLiteDatabase.OPEN_READWRITE);
        }
    }

    public void closeDB() {
        if (sqliteManager != null &&
                sqliteManager.isOpen()) {
//            sqliteManager.close();
        }
    }

    public List<DataAnUong> getListAnUong() {
        dataAnUongs = new ArrayList<>();
        openDB();
        c = sqliteManager.rawQuery(
                "SELECT * FROM regime order by Name",
                null);
        if (!c.isAfterLast()) {
            int iName = c.getColumnIndex("Name");
            int iLink = c.getColumnIndex("URL");
            c.moveToFirst();
            while (!c.isAfterLast()){
                name = c.getString(iName);
                link = c.getString(iLink);
                dataAnUong = new DataAnUong();
                dataAnUong.setLink(link);
                dataAnUong.setName(name);
                dataAnUongs.add(dataAnUong);
                c.moveToNext();
            }
            c.close();

        }
        closeDB();
        return dataAnUongs;
    }

    public List<DatabaseDongY> getListDongY(int number) {
        databases = new ArrayList<>();
        openDB();
        c = sqliteManager.rawQuery(
                "SELECT * FROM Items where Idc =" + number,
                null);

        if (!c.isAfterLast()) {
            int indexId = c.getColumnIndex("Idc");
            int iName = c.getColumnIndex("Name");
            int iLink = c.getColumnIndex("Link");
            c.moveToFirst();
            while (!c.isAfterLast()) {
                id = c.getInt(indexId);
                name = c.getString(iName);
                link = c.getString(iLink);
                database = new DatabaseDongY();
                database.setId(id);
                database.setName(name);
                database.setLink(link);
                databases.add(database);
                c.moveToNext();
            }
            c.close();
        }
        closeDB();
        return databases;
    }

}
